package ProyectoFinalpkg;
import java.util.Random;
import java.util.Scanner;
//Alex Alchapar
public class JuegoDeLaVida {
	static char opcio;
	static int mida;
	static boolean opcioA = false;
	static char mapa[][];
	static int mapa2[][];
	public static void main(String[] args) {
		PresentaTitols();
		do {
			PresentaMenu();
			opcio = DemanaCaracter();
			Executa(opcio);
		} while (opcio != 'd');
	}
	static void PresentaTitols() {
		System.out.print("Benvingut al Joc de la vida");
	}
	static void PresentaMenu() {
		System.out.println();
		System.out.println("a) Incialitza tauler");
		System.out.println("b) Visualitzar tauler");
		System.out.println("c) Iterar");
		System.out.println("d) Sortir");
	}
	static char DemanaCaracter() {
		Scanner lector = new Scanner(System.in);
		boolean correcte = false;
		char caracterEntrado = 0;
		while (correcte == false) {
			System.out.println("Introdueix una opcio: ");
			caracterEntrado = lector.next().charAt(0);
			if (caracterEntrado != 'a' && caracterEntrado != 'b' && caracterEntrado != 'c' && caracterEntrado != 'd') {
				System.out.println("Error: No has introduit cap de les opcions donades");
				lector.nextLine();
			} else
				correcte = true;
		}
		return caracterEntrado;
	}
	static void Executa(char opcio) {
		switch (opcio) {
		case 'a':
			IniciarMatriu();
			break;
		case 'b':
			VeureMatriu();
			break;
		case 'c':
			Iterar();
			break;
		case 'd':
			Adeu();
		}
	}
	static void IniciarMatriu() {
		boolean correcte2 = false;
		Scanner lector = new Scanner(System.in);
		Random random = new Random();
		while (!correcte2) {
			System.out.println("Introdueix la mida del tauler (4-10):");
			mida = lector.nextInt();
			if (mida <= 3 || mida >= 11) {
				System.out.println("Error: La mida ha de ser entre 4 i 10!");
			} else {
				mapa = new char[mida][mida];
				correcte2 = true;
				opcioA = true;
				for (int i = 0; i < mapa.length; i++) {
					for (int j = 0; j < mapa[i].length; j++) {
						mapa[i][j] = '-';
					}
				}
				int totalcaselles = mida * mida;
				int Rang1 = totalcaselles / 2;
				int Rang2 = totalcaselles / 4;
				int vives = (int) Math.floor(Math.random() * (Rang1 - (Rang2)) + (Rang2));
				System.out.println("vives: " + vives);
				for (int i = 0; i < vives; i++) {
					boolean posiciook = false;
					do {
						int random1 = random.nextInt(mida);
						int random2 = random.nextInt(mida);
						if (mapa[random1][random2] != '+') {
							mapa[random1][random2] = '+';
							posiciook = true;
						}
					} while (posiciook == false);
				}

			}
			lector.nextLine();
		}
	}
	static void VeureMatriu() {
		if (!opcioA) {
			System.out.println("No s'ha inicialitzat el tauler, passa primer per l'opci� a)");
		} else {
			for (int i = 0; i < mapa.length; i++) {
				for (int j = 0; j < mapa[i].length; j++) {
					System.out.print(mapa[i][j] + "\t");
				}
				System.out.println();
			}
		}
	}
	static void Iterar() {
		Scanner Lector = new Scanner(System.in);
		boolean correcte = false;
		boolean iterarok = false;
		int iteracions = 0;
		if (!opcioA) {
			System.out.println("No s'ha inicialitzat el tauler, passa primer per l'opci� a)");
		} else {
			for (int i = 0; i < mapa.length; i++) {
				for (int j = 0; j < mapa[i].length; j++) {
					System.out.print(mapa[i][j] + "\t");
				}
				System.out.println();
			}
			while (!correcte) {
				try {
					do {
						System.out.println("Quantes iteracions vols fer?");
						iteracions = Lector.nextInt();
						if (iteracions > 100 || iteracions < 0) {
							System.out.println("El n�mero d'iteracions ha de ser entre 0 i 100!");
						} else
							iterarok = true;
					} while (!iterarok);
					correcte = true;
				} catch (Exception e) {
					System.out.println("No esta permes el valor introduit!");
					Lector.nextLine();
				}
			}

			int veins = 0;
			mapa2 = new int[mida][mida];
			for (int x = 0; x < iteracions; x++) {
				for (int i = 0; i < mapa.length; i++) {
					for (int j = 0; j < mapa[i].length; j++) {
						veins = 0;

						if (i - 1 >= 0 && j - 1 >= 0 && mapa[i - 1][j - 1] == '+') {
							veins++;
						}

						if (i - 1 >= 0 && mapa[i - 1][j] == '+') {
							veins++;
						}
						if (i - 1 >= 0 && j + 1 < mapa[i].length && mapa[i - 1][j + 1] == '+') {
							veins++;
						}
						if (j - 1 >= 0 && mapa[i][j - 1] == '+') {
							veins++;
						}
						if (j + 1 < mapa[i].length && mapa[i][j + 1] == '+') {
							veins++;
						}
						if (i + 1 < mapa.length && j - 1 >= 0 && mapa[i + 1][j - 1] == '+') {
							veins++;
						}
						if (i + 1 < mapa.length && mapa[i + 1][j] == '+') {
							veins++;
						}
						if (i + 1 < mapa.length && j + 1 < mapa[i].length && mapa[i + 1][j + 1] == '+') {
							veins++;
						}

						mapa2[i][j] = veins;
					}
				}
				for (int i = 0; i < mapa2.length; i++) {
					for (int j = 0; j < mapa2[i].length; j++) {
						if (mapa2[i][j] == 3 && mapa[i][j] == '-') {
							mapa[i][j] = '+';
						}
						if (mapa[i][j] == '+' && (mapa2[i][j] <= 1 || mapa2[i][j] >= 4)) {
							mapa[i][j] = '-';
						}
						System.out.print(mapa[i][j] + "\t");
					}
				}
			}
		}
	}
	static void Adeu() {
		System.out.println("Adeu!");
	}
}